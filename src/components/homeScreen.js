import React from "react";
import { View, Button, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";

const HomeScreen = () =>{
    const navigation = useNavigation();
    const handleUser = ()=>{
        navigation.navigate('User')
    }
    return(
        <View>
            <Button title="Ver Usuários" onPress={handleUser}>

            </Button>
        </View>
    );
}

export default HomeScreen