import React, {useEffect, useState} from "react";
import { View, Button, Text, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";

const UserScreen = ()=>{
const [users, setUsers] = useState([]);
useEffect(()=>{
    getUser()
}, [])

    async function getUser(){
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/users');
            setUsers(response.data);
        } catch (error) {
            console.error(error)
        }
    }

return(
    <View>  
        <Text>  
            Lista de Usuarios
        </Text>

        <FlatList
        data={users}
        keyExtractor={(item)=> item.id.toString()} 
        renderItem={({item})=> (
            <View>
                <Text>
                    {item.name}
                </Text>
            </View>
        )}

        />
    </View>
)
};

export default UserScreen
